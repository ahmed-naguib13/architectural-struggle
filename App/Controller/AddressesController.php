<?php
namespace App\Controller;

use NoStruggle\Lib\Controller\Controller;
use NoStruggle\Lib\Response\Response;
use NoStruggle\Lib\Response\JsonResponse;
use NoStruggle\Lib\Exception\NoStruggleException;
use App\Model\Entity\Address;

/**
 * Class AddressesController
 * @package App\Controller
 */
class AddressesController extends Controller
{
    /**
     * @param $id
     * @return JsonResponse
     * @throws NoStruggleException
     */
    public function getAddressAction($id)
    {
        $addressArray = $this->getAddressesDb()->getOneById($id);

        if ($addressArray) {
            // create Address object
            $address = new Address($addressArray[0], $addressArray[1], $addressArray[2], $id);
            $response = new JsonResponse(json_encode($address));
            return $response;
        }

        throw new NoStruggleException('Address Not Found', Response::HTTP_NOT_FOUND);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @throws NoStruggleException
     */
    public function updateAddressAction($id)
    {
        $db = $this->getAddressesDb();
        $addressArray = $db->getOneById($id);
        if ($addressArray) {
            $postContent = $this->getRequest()->getContent();
            if (empty($postContent)) {
                throw new NoStruggleException('Invalid Request', Response::HTTP_BAD_REQUEST);
            }
            $requestArray = json_decode($postContent, true);
            $entity = $db->setById($id, $requestArray);
            if ($entity) {
                $db->save();
                $response = new JsonResponse('', Response::HTTP_NO_CONTENT);
                return $response;
            }
        } else {
            throw new NoStruggleException('Address Not Found', Response::HTTP_NOT_FOUND);
        }

        throw new NoStruggleException('Invalid Request', Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @throws NoStruggleException
     */
    public function deleteAddressAction($id)
    {
        $db = $this->getAddressesDb();
        $addressArray = $db->getOneById($id);
        if ($addressArray) {
            $removed = $db->removeById($id);
            if ($removed) {
                $db->save();
                $response = new JsonResponse('', Response::HTTP_NO_CONTENT);
                return $response;
            } else {
                throw new NoStruggleException('Something went wrong. Address was not deleted');
            }

        } else {
            throw new NoStruggleException('Address Not Found', Response::HTTP_NOT_FOUND);
        }

        throw new NoStruggleException('Invalid Request', Response::HTTP_BAD_REQUEST);
    }

    /**
     * @return JsonResponse
     * @throws NoStruggleException
     */
    public function getAddressesAction()
    {
        $allAddresses = $this->getAddressesDb()->getAll();

        // create Address object
        $addressList = array();
        foreach ((array)$allAddresses as $id => $addressArray) {
            $addressList[] = new Address($addressArray[0], $addressArray[1], $addressArray[2], $id);
        }

        $response = new JsonResponse(
            json_encode(
                array(
                    'addresses' => $addressList
                )
            )
        );

        return $response;
    }

    /**
     * @return JsonResponse
     * @throws NoStruggleException
     */
    public function createAddressAction()
    {
        $postContent = $this->getRequest()->getContent();
        if (empty($postContent)) {
            throw new NoStruggleException('Invalid Request', Response::HTTP_BAD_REQUEST);
        }
        $requestArray = json_decode($postContent, true);
        $address = new Address(
            $requestArray['name'],
            $requestArray['phone'],
            $requestArray['address']
        );
        $addressDb = $this->getAddressesDb();
        $id = $addressDb->addNew(
            array(
                $address->getName(),
                $address->getPhone(),
                $address->getAddress()
            )
        );
        if ($id >= 0) {
            // save data actually to the file
            $addressDb->save();
            $address->setId($id);
            $response = new JsonResponse(json_encode($address), Response::HTTP_CREATED);
            return $response;
        }
        throw new NoStruggleException('Address was not saved', Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @return mixed
     */
    private function getAddressesDb()
    {
        $addressesDb = $this->get('addressesDb');
        $addressesDb->connect();

        return $addressesDb;
    }
}