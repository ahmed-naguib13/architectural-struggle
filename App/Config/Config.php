<?php
namespace App\Config;

/**
 * Class Config
 * @package App\Config
 */
class Config
{
    /**
     * @return array
     */
    public function getParameters()
    {
        return array(
            'data_sources' => array(
                array(
                    'name' => 'addressesDb',
                    'type' => 'csv',
                    'path' => 'Data/examples.csv'
                )
            )
        );
    }
}