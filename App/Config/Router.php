<?php
namespace App\Config;

/**
 * Class Router
 * @package App\Config
 */
class Router
{
    /**
     * @return array
     */
    public function getRoutes()
    {
        return array(
            array(
                'name' => 'singleAddress',
                'method' => 'get',
                'pattern' => '/addresses/(\d+)',
                'callback' => 'Addresses:getAddress'
            ),
            array(
                'name' => 'allAddresses',
                'method' => 'get',
                'pattern' => '/addresses',
                'callback' => 'Addresses:getAddresses'
            ),
            array(
                'name' => 'createAddress',
                'method' => 'post',
                'pattern' => '/addresses',
                'callback' => 'Addresses:createAddress'
            ),
            array(
                'name' => 'updateAddress',
                'method' => 'put',
                'pattern' => '/addresses/(\d+)',
                'callback' => 'Addresses:updateAddress'
            ),
            array(
                'name' => 'deleteAddress',
                'method' => 'delete',
                'pattern' => '/addresses/(\d+)',
                'callback' => 'Addresses:deleteAddress'
            )
        );
    }
}