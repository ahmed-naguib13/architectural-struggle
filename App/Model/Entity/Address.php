<?php
namespace App\Model\Entity;

/**
 * Class Address
 * @package App\Model\Entity
 */
class Address implements \JsonSerializable
{
    private $id;
    private $name;
    private $phone;
    private $address;

    /**
     * Address constructor.
     * @param $name
     * @param $phone
     * @param $address
     * @param $id
     */
    public function __construct($name, $phone, $address, $id = null)
    {
        $this->name = $name;
        $this->phone = $phone;
        $this->address = $address;
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'address' => $this->address
        );
    }
}