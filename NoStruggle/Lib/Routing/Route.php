<?php
namespace NoStruggle\Lib\Routing;

/**
 * Class Route
 * @package NoStruggle\Lib\Routing
 */
class Route
{
    const PROPERTY_NAME = 'name';
    const PROPERTY_PATTERN = 'pattern';
    const PROPERTY_CALLBACK = 'callback';
    const PROPERTY_METHOD = 'method';

    private $name;
    private $pattern;
    private $callback;
    private $method;

    /**
     * Route constructor.
     * @param $name
     * @param $pattern
     * @param $callback
     * @param $method
     */
    public function __construct($name, $pattern, $callback, $method)
    {
        $this->name = $name;
        $this->pattern = $pattern;
        $this->callback = $callback;
        $this->method = $method;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * @param mixed $pattern
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;
    }

    /**
     * @return mixed
     */
    public function getCallback()
    {
        return $this->callback;
    }

    /**
     * @param mixed $callback
     */
    public function setCallback($callback)
    {
        $this->callback = $callback;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return strtolower($this->method);
    }

    /**
     * @param mixed $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return bool
     */
    public function isCallable()
    {
        return is_object($this->callback) && ($this->callback instanceof \Closure);
    }
}