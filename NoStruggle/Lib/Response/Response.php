<?php
namespace NoStruggle\Lib\Response;

/**
 * Class Response
 * @package NoStruggle\Lib\Response
 */
class Response
{
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_ACCEPTED = 202;
    const HTTP_NO_CONTENT = 204;
    const HTTP_MOVED_PERMANENTLY = 301;
    const HTTP_FOUND = 302;
    const HTTP_NOT_MODIFIED = 304;
    const HTTP_TEMPORARY_REDIRECT = 307;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_PAYMENT_REQUIRED = 402;
    const HTTP_FORBIDDEN = 403;
    const HTTP_NOT_FOUND = 404;
    const HTTP_METHOD_NOT_ALLOWED = 405;
    const HTTP_NOT_ACCEPTABLE = 406;
    const HTTP_INTERNAL_SERVER_ERROR = 500;
    const HTTP_NOT_IMPLEMENTED = 501;
    const HTTP_BAD_GATEWAY = 502;
    const HTTP_SERVICE_UNAVAILABLE = 503;
    const HTTP_GATEWAY_TIMEOUT = 504;
    const HTTP_VERSION = '1.1';

    public static $statusTexts = array(
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        204 => 'No Content',
        301 => 'Moved Permanently',
        302 => 'Found',
        304 => 'Not Modified',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout'
    );
    /**
     * @var
     */
    private $content;

    private $statusCode;

    private $headers;

    /**
     * Response constructor.
     * @param $content
     * @param $statusCode
     * @param $headers
     */
    public function __construct($content, $statusCode = self::HTTP_OK, $headers = array())
    {
        $this->content = $content;
        $this->statusCode = $statusCode;
        $this->headers = $headers;
    }

    /**
     *
     */
    public function send()
    {
        $this->sendHeaders();
        $this->sendContent();

    }

    /**
     * sends the content of the response to the browser
     */
    public function sendContent()
    {
        echo $this->content;
    }

    /**
     * sends the headers of the response to the client
     */
    public function sendHeaders()
    {
        if (headers_sent()) {
            return;
        }

        $headers = (array)$this->getHeaders();
        // go through headers and add them on by one
        foreach ($headers as $name => $value) {
            header($name . ': ' . $value, false, $this->statusCode);
        }

        // add the final http status header
        header('HTTP/' . self::HTTP_VERSION . ' ' . $this->statusCode . ' ' . $this->getStatusText(), true, $this->statusCode);
    }

    /**
     * @return statusText or null
     */
    public function getStatusText()
    {
        if (isset(self::$statusTexts[$this->statusCode])) {
            return self::$statusTexts[$this->statusCode];
        }

        return null;
    }
    /**
     * @param $name
     * @param $value
     */
    public function addHeader($name, $value)
    {
        // If it exists we override it
        $this->headers[$name] = $value;
    }


    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param mixed $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }
}