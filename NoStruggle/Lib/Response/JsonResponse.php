<?php
namespace NoStruggle\Lib\Response;

/**
 * Class JsonResponse
 * @package NoStruggle\Lib\Response
 */
class JsonResponse extends Response
{
    public function __construct($content, $statusCode = self::HTTP_OK, $headers = array())
    {
        parent::__construct($content, $statusCode, $headers);
        // By default add the content type header
        $this->addHeader('Content-Type', 'Application/json');
    }
}