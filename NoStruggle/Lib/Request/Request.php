<?php
namespace NoStruggle\Lib\Request;

/**
 * Class Request
 * @package NoStruggle\Lib\Request
 */
class Request
{
    /**
     * @var
     */
    private $uri;
    /**
     * @var
     */
    private $host;
    /**
     * @var
     */
    private $requestMethod;

    /**
     * @var
     */
    private $queryStringCollection = array();

    /**
     * @var
     */
    private $postParameters = array();

    /**
     * @var
     */
    private $content = '';

    /**
     * @var
     */
    private $headers = array();

    /**
     * Request constructor.
     * @param $host
     * @param $uri
     * @param $requestMethod
     */
    public function __construct($host, $uri, $requestMethod)
    {
        $this->host = $host;
        $this->uri = $uri;
        $this->requestMethod = $requestMethod;
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @return mixed
     */
    public function getQueryStringCollection()
    {
        return $this->queryStringCollection;
    }

    /**
     * @param mixed $queryStringCollection
     */
    public function setQueryStringCollection($queryStringCollection)
    {
        $this->queryStringCollection = $queryStringCollection;
    }

    /**
     * @param mixed $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return mixed
     */
    public function getRequestMethod()
    {
        return strtolower($this->requestMethod);
    }

    /**
     * @param mixed $requestMethod
     */
    public function setRequestMethod($requestMethod)
    {
        $this->requestMethod = $requestMethod;
    }

    /**
     * @return mixed
     */
    public function getPostParameters()
    {
        return $this->postParameters;
    }

    /**
     * @param mixed $postParameters
     */
    public function setPostParameters($postParameters)
    {
        $this->postParameters = $postParameters;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param mixed $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    /**
     * @param $key
     * @param null $defaultValue
     * @return null
     */
    public function get($key, $defaultValue = null)
    {
        if (isset($this->queryStringCollection[$key])) {
            return $this->queryStringCollection[$key];
        }

        return $defaultValue;
    }
}