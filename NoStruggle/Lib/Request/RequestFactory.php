<?php
namespace NoStruggle\Lib\Request;

/**
 * Class RequestFactory
 * @package NoStruggle\Lib\Request
 */
class RequestFactory
{
    /**
     * @return Request
     */
    static public function getInstance()
    {
        $host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
        $getCollection = empty($_GET) ? array() : $_GET;
        $requestMethod = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD']: null;
        $postCollection = isset($_POST) ? $_POST : '';

        // get the url after the host , we are only interested in the part after app.php
        $requestUri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
        $requestParts = explode('app.php',$requestUri);
        $requestUri = isset($requestParts[1]) ? $requestParts[1] : $requestUri;

        $headers = getallheaders();
        $postBody = file_get_contents('php://input');

        // create a new request and fill all the data
        $request = new Request($host, $requestUri, $requestMethod);
        $request->setQueryStringCollection($getCollection);
        $request->setPostParameters($postCollection);
        $request->setContent($postBody);
        $request->setHeaders($headers);

        return $request;
    }
}