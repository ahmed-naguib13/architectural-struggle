<?php
namespace NoStruggle\Lib\Application;

use App\Config\Config;
use App\Config\Router;
use NoStruggle\Lib\EntityManager\CsvManager as CsvManager;
use NoStruggle\Lib\EntityManager\DataSource;
use NoStruggle\Lib\Request\Request;
use NoStruggle\Lib\Response\Response;
use NoStruggle\Lib\Routing\Route;
use NoStruggle\Lib\Exception\NoStruggleException;

/**
 * Class Application
 * @package NoStruggle\Lib\Application
 */
class Application
{
    const APP_DIR = 'App';
    const CONTROLLER_DIR = 'Controller';
    const MODEL_DIR = 'Model';
    const VIEW_DIR = 'View';
    const CONFIG_DATASOURCES = 'data_sources';
    const ENTITY_MANAGER_DIR = 'EntityManager';
    const BASE_NAMESPACE = 'NoStruggle\\Lib\\';

    /**
     * @var array
     */
    private $routes;

    /**
     * @var array
     */
    private $services;

    /**
     * Application constructor.
     */
    public function __construct()
    {
        $this->routes = array();
        $this->services = array();
        $this->loadConfig();
        $this->loadRoutes();
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws NoStruggleException
     */
    public function handle(Request $request)
    {
        // load the app config
        $uri = $request->getUri();
        // match the request url with the corresponding route from config
        $routes = $this->getRoutes();
        $matchedRoute = null;

        $matches = array();
        foreach ($routes as $route) {
            $regex = '/' . str_replace('/', '\/', $route->getPattern()) . '/';
            $match = preg_match($regex, $uri, $matches);

            if (empty($match))
                continue;

            // if there is a match we should check the method also
            if ($route->getMethod() == $request->getRequestMethod()) {
                $matchedRoute = $route;
                break;
            }
        }
        // Initialize the controller and call the action
        if (empty($matchedRoute)) {
            throw new NoStruggleException("No route was found for $uri", Response::HTTP_NOT_FOUND);
        }

        // ignore first one as it's the whole array
        unset($matches[0]);
        // render the response based on the config we have
        if ($matchedRoute->isCallable()) {
            $response = call_user_func_array($matchedRoute->getCallback(), $matches);
        } else {
            $controllerAction = explode(':', $matchedRoute->getCallback());
            $controllerClass = $this->getControllersNameSpace() . $controllerAction[0] . 'Controller';
            $actionName = $controllerAction[1];
            $controller = new $controllerClass($request, $this);
            $action = $actionName . 'Action';
            $response = call_user_func_array(array($controller, $action), $matches);
        }

        if ($response instanceof Response) {
            return $response;
        }

        throw new NoStruggleException('Invalid response type. Controller must return object of type Response');
    }

    /**
     * @return mixed
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @param mixed $routes
     */
    public function setRoutes($routes)
    {
        $this->routes = $routes;
    }

    /**
     * @param $method
     * @param $pattern
     * @param $callback
     * @param $name
     * @throws NoStruggleException
     */
    public function addRoute($method, $pattern, $callback, $name)
    {
        if (isset($this->routes[$name])) {
            throw new NoStruggleException("Route $name already exists");
        }
        $this->routes[$name] = new Route($name, $pattern, $callback, $method);
    }

    /**
     * @return string
     */
    private function getControllersNameSpace()
    {
        return self::APP_DIR . '\\' . self::CONTROLLER_DIR . '\\';
    }

    /**
     * @return string
     */
    private function getEntityManagersNameSpace()
    {
        return self::BASE_NAMESPACE . self::ENTITY_MANAGER_DIR . '\\';
    }

    private function loadConfig()
    {
        $config = new Config();
        $parameters = $config->getParameters();
        $dataSources = isset($parameters[self::CONFIG_DATASOURCES]) ? $parameters[self::CONFIG_DATASOURCES] : array();
        foreach ($dataSources as $dataSource) {
            $this->addDataSource(
                $dataSource[DataSource::PROPERTY_NAME],
                $dataSource[DataSource::PROPERTY_TYPE],
                $dataSource[DataSource::PROPERTY_PATH]
            );
        }
    }

    private function addDataSource($name, $type, $path)
    {
        $dataSourceClass = $this->getEntityManagersNameSpace() . ucfirst($type) . DataSource::CLASS_POSTFIX;
        $dataSource = new $dataSourceClass($name, $type, $path);
        $this->services[$name] = $dataSource;
    }

    private function loadRoutes()
    {
        $router = new Router();
        $routes = $router->getRoutes();
        foreach ($routes as $route) {
            $this->addRoute(
                $route[Route::PROPERTY_METHOD],
                $route[Route::PROPERTY_PATTERN],
                $route[Route::PROPERTY_CALLBACK],
                $route[Route::PROPERTY_NAME]
            );
        }
    }

    /**
     * @return array
     */
    public function getServices()
    {
        return $this->services;
    }
}