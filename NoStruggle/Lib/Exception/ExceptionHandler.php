<?php
use NoStruggle\Lib\Response\Response;
use NoStruggle\Lib\Exception\NoStruggleException;

function exceptionHandler(NoStruggleException $exception)
{
    $statusCode = $exception->getStatusCode() ? $exception->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
    $response = new Response(
        json_encode(
            array(
                'message' => $exception->getMessage()
            )
        ), $statusCode);
    $response->send();
}

set_exception_handler('exceptionHandler');