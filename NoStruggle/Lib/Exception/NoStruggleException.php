<?php
namespace NoStruggle\Lib\Exception;

use NoStruggle\Lib\Response\Response;

/**
 * Class NoStruggleException
 * @package NoStruggle\Lib\Exception
 */
class NoStruggleException extends \Exception
{
    private $statusCode;

    /**
     * @param string $message
     * @param null $statusCode
     */
    public function __construct($message, $statusCode = null)
    {
        parent::__construct($message);
        $this->statusCode = $statusCode;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }
}