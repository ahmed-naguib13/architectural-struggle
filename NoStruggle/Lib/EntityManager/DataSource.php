<?php
namespace NoStruggle\Lib\EntityManager;

/**
 * Class DataSource
 * @package NoStruggle\Lib\EntityManager
 */
class DataSource
{
    const PROPERTY_NAME = 'name';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_PATH = 'path';
    const CLASS_POSTFIX = 'Manager';

    private $name;
    private $type;
    private $path;

    /**
     * DataSource constructor.
     * @param $name
     * @param $type
     * @param $path
     */
    public function __construct($name, $type, $path)
    {
        $this->name = $name;
        $this->type = $type;
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }
}