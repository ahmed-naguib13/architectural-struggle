<?php
namespace NoStruggle\Lib\EntityManager;

/**
 * Interface SourceConnectionInterface
 * @package NoStruggle\Lib\EntityManager
 */
interface SourceConnectionInterface
{
    /**
     * @return mixed
     */
    public function connect();
}