<?php
namespace NoStruggle\Lib\EntityManager;

use NoStruggle\Lib\Exception\NoStruggleException;

class CsvManager extends DataSource implements SourceConnectionInterface
{
    private $collection = array();

    /**
     * @throws NoStruggleException
     */
    public function connect()
    {
        $file = $this->getFile();
        $offset = 0;
        while (($line = fgetcsv($file)) !== FALSE) {
            $this->collection[$offset++] = $line;
        }

        fclose($file);
    }

    /**
     * @param string $mode
     * @return resource
     * @throws NoStruggleException
     */
    private function getFile($mode = 'r')
    {
        $path = ROOT_DIR . '/' . $this->getPath();

        if (!file_exists($path)) {
            throw new NoStruggleException('File was not found at' . $this->getPath());
        }

        $file = fopen($path, $mode);

        return $file;
    }

    /**
     * @return int
     */
    public function getRowCount()
    {
        return count($this->collection);
    }

    /**
     * @return int
     */
    public function getNewId()
    {
        return $this->getRowCount();
    }

    /**
     * @param $id
     * @return null
     */
    public function getOneById($id)
    {
        if (isset($this->collection[$id])) {
            return $this->collection[$id];
        }

        return null;
    }

    /**
     * @param $id
     * @param $entity
     * @return null
     */
    public function setById($id, $entity)
    {
        if (isset($this->collection[$id])) {
            return $this->collection[$id] = $entity;
        }

        return null;
    }

    /**
     * @param $id
     * @return bool|null
     */
    public function removeById($id)
    {
        if (isset($this->collection[$id])) {
            unset($this->collection[$id]);

            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->collection;
    }

    /**
     * @param $entity
     * @return int
     */
    public function addNew($entity)
    {
        $offset = $this->getNewId();
        $this->collection[$offset] = $entity;

        return $offset;
    }

    /**
     * saves the collection to the file
     * @throws NoStruggleException
     */
    public function save()
    {
        $file = $this->getFile('w+');

        foreach ($this->collection as $line) {
            fputcsv($file, $line);
        }

        fclose($file);
    }

    /**
     * @return mixed
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * @param mixed $collection
     */
    public function setCollection($collection)
    {
        $this->collection = $collection;
    }
}