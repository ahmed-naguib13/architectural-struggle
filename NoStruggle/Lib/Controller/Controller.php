<?php
namespace NoStruggle\Lib\Controller;

use NoStruggle\Lib\Application\Application;
use NoStruggle\Lib\Request\Request;

/**
 * Class Controller
 * @package NoStruggle\Lib\Controller
 */
class Controller
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var null
     */
    private $response;

    /**
     * @var null
     */
    private $app;

    /**
     * @param Request $request
     */
    public function __construct(Request $request, Application $app)
    {
        $this->request = $request;
        $this->response = null;
        $this->app = $app;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * @param $key
     * @param null $default
     * @return null
     */
    public function get($key, $default = null)
    {
        $services = $this->app->getServices();
        if (isset($services[$key])) {
            return $services[$key];
        }

        return $default;
    }
}