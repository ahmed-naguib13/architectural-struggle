<?php
define('ROOT_DIR', __DIR__);

require 'autoloader.php';
require 'NoStruggle/Lib/Exception/ExceptionHandler.php';

use NoStruggle\Lib\Request\RequestFactory;
use NoStruggle\Lib\Application\Application;

$app = new Application();
$request = RequestFactory::getInstance();
$response = $app->handle($request);
$response->send();