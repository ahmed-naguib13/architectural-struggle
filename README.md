This is an example project to work with the mini framework NoStruggle

Requirements:
--
    PHP >= 5.4
    
Folders Structure is as follows :
     

    App
    	Config
        	Config.php #Here we add the configurations for the project (dbconnections , parameters , etc..)
        	Router.php #Here you can define your application routes
    	Controller
        	DefaultController.php
    	Model
        	Entity
           		 MyEntity.php
    	View
        	Default
            	index.html

For config files, basic php arrays are used ..

Example for Config:

    return array(
        'data_sources' => array(
            array(
                'name' => 'usersDb',
                'type' => 'csv',
                'path' => 'Data/users.csv'
            )
        )
    );

For routes , also basic php arrays are used ..

Example for Router:

    return array(
      array(
          'name' => 'singleUser',
          'method' => 'get',
          'pattern' => '/users/(\d+)',
          'callback' => 'Users:getUser'
      ),
    );

Currently the app exists in the App folder and the working urls should be as follows :


    GET app.php/addresses
    Description: get all addresses

    GET app.php/addresses/1
    Description: get single address

    POST app.php/addresses
    Description: create new a address

    PUT app.php/addresses/1
    Description: update an address

    DELETE app.php/addresses/1
    Description: delete an address

TODOs:
    - implement yaml support for config
    - add extra data sources like SQLite3